class MyHeader extends HTMLElement{
    connectedCallback() {
        this.innerHTML =
            ' <header>\n' +
            '        <nav class="navbar navbar-expand-lg navbar-light bg-info">\n' +
            '    <div class="container">\n' +
            '        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">\n' +
            '            <span class="navbar-toggler-icon"></span>\n' +
            '        </button>\n' +
            '        <div class="collapse navbar-collapse" id="navbarNavDropdown">\n' +
            '            <ul class="navbar-nav">\n' +
            '                <li class="nav-item active">\n' +
            '                    <a href="Home.html"><img src="img/music.png" class="music-logo mt-1"></a>\n' +
            '                </li>\n' +
            '                   <li class="nav-item active ml-5">\n' +
            '                    <a class="nav-link text-white" href="Home.html"><i class="fa-solid fa-house"></i> HOME <span class="sr-only">(current)</span></a>\n' +
            '                </li>\n' +
            '                <li class="nav-item active ml-5">\n' +
            '                    <a class="nav-link text-white " href="#"><i class="fa-solid fa-circle-play"></i> My Playlist</a>\n' +
            '                </li>\n' +
            '                <li class="nav-item active ml-5 ">\n' +
            '                    <a class="nav-link text-white" href="#"><i class="fa-solid fa-music"></i> My Music</a>\n' +
            '                </li>\n' +
            '                <li class="nav-item active ml-5 ">\n' +
            '                    <a class="nav-link text-white" href="#"><i class="fa-solid fa-podcast"></i> Podcast</a>\n' +
            '                </li>\n' +
            '                <li class="nav-item active ml-5  dropdown">\n' +
            '                    <a class="nav-link text-white dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\n' +
            '                        MORE\n' +
            '                    </a>\n' +
            '                    <div class="dropdown-menu bg-info" aria-labelledby="navbarDropdownMenuLink">\n' +
            '                        <a class="dropdown-item text-white" href="About.html"><i class="fa-solid fa-users"></i> ABOUT US</a>\n' +
            '                        <a class="dropdown-item text-white" href="Contact.html"><i class="fa-solid fa-address-card"></i> CONTACT US</a>\n' +
            '                    </div>\n' +
            '                </li>\n' +
            '            </ul>\n' +
            '        </div>\' '+
            '<form class="form-inline searchbar">\n' +
            '      <input class="form-control mr-sm-2 form-search" type="search" placeholder="Search Here..." aria-label="Search">\n' +
            '<n></n><i class="fa-solid fa-magnifying-glass icon"></i>'+
            '    </form>\n'+
            '    </div>\n' +
            '</nav>\n' +
            '    </header>'
    }
}
customElements.define('my-header', MyHeader)


class MyFooter extends HTMLElement{
    connectedCallback() {
        this.innerHTML ='<footer>\n' +
            '        <div class="footer pt-0">\n' +
            '\n' +
            '            <div class="container">\n' +
            '                <div class="footer-follow">\n' +
            '                    <div class="row">\n' +
            '                        <div class="col-6 follow-body2 pt-3">\n' +
            '                            <p> &copy; Copyrights 1997 - 2022. All rights reserved.</p>\n' +
            '                        </div>\n' +
            '                        <div class="col-6 d-flex align-items-center flex-end">\n' +
            '                            <h4>Follow us on:&ensp;</h4>\n' +
            '                            <ul class="social list-unstyled mb-0">\n' +
            '                                <li><a href="#" class="mr-2"><img src="img/fb-logo-35x35.png"> </a></li>\n' +
            '                                <li><a href="#" class="mr-2"><img src="img/instagram-logo-35x35.png"> </a></li>\n' +
            '                                <li><a href="#"><img src="img/twitter-logo-35x35.png"> </a></li>\n' +
            '                            </ul>\n' +
            '                        </div>\n' +
            '                    </div>\n' +
            '                </div>\n' +
            '            </div>\n' +
            '        </div>\n' +
            '    <footer>'

    }
}
customElements.define('my-footer', MyFooter)